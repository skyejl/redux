const initialState = {
    payload: {}
};
export default (state = initialState, action) => {
    switch (action.type) {
        case 'GET_USER_DETAIL':
            return {
                ...state,
                payload: action.payload
            };

        default:
            return state
    }
}
