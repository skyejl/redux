export const getUserProfile = () => (dispatch) => {
    fetch("http://localhost:8080/api/user-profiles/2")
        .then(response => response.json())
        .then(result => {
            dispatch({
                type: 'GET_USER_DETAIL',
                payload: {name:result.name}
            })
        })
};


