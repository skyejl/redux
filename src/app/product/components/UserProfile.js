import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getUserProfile} from "../actions/userActions";
import '../../home.less'

class UserProfile extends Component {

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getUserProfile();
    }

    render() {
        const user = this.props.payload;
        return (
            <div>
                <h1>User Profile</h1>
                <p>
                    <label>User Name: </label>
                    <span>{user.name}</span>
                </p>
                 <p>
                    <label>Gender: </label>
                    <span>male</span>
                </p>

                <p>这是 用户{this.props.match.params.id}</p>
                <Link to='/'>back to home</Link>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    payload: state.user.payload
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getUserProfile
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);


